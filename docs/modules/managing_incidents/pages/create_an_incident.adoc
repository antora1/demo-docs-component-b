:imagesdir: ./images

==== Creating a New Incident

An incident is created when the service provider becomes aware of a problem that can affect large number of customers.

To create an incident:

. Retrieve the customer using the _Global Search_ bar.
. Click the _View_ icon.
. Validate the customer or contact.
. Go to _Case Panel_
. Click the _Issues_ tab.
. Select the _Troubleshooting_ option.
. Select the _Reason Code_ from the dropdown.
. Fill in the address of the location that is facing the problem.
. Select _New Incident_ using the toggle button.
. Select the trouble occurrence date.
. Select the _Time of Day_ using the dropdown.
. Select the _Frequency_ of the trouble occurrence using the dropdown.
. Enter the description.
. Enter the _Contact Information_.
. Select _Yes_ or _No_ radial button to notify on closure depending on the requirement.

    NOTE: To enable notify on closure, select a notification method Pop-up Message or Email using the dropdown. 

. Click the *Save* button.