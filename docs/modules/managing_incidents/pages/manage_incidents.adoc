:imagesdir: ./images

=== Managing Incidents

An Incident is a problem with the service provider’s infrastructure that could affect many customers.

An Incident is created by an authorized user when a problem is reported, and then it can be linked to Troubleshooting issues raised by customers. On the Troubleshooting issue, the customer can request to be notified when the Incident is resolved.

When the Incident is resolved, the system will attempt to close all Troubleshooting issues linked to the Incident and then notify the customers who have requested a notification.

This section describes managing incidents that can affect a large number of a service provider's customers.