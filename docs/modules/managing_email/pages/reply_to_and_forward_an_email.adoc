:imagesdir: ./images

===== Reply To and Forward Emails

User can reply and forward customer emails by using the appropriate buttons:

To reply or forward an email:

. Retrieve the case using the _Global Search_ bar.
. Click the *View* icon to open the case panel.
. Click the *WORK ON CASE* button.
. Select the _Email_ tab.
. Open the required email from the list.
  ** Click the _Reply_ icon to provide a reply to an email.
  ** Click the _Forward_ icon to forward an email.