:imagesdir: ./images

==== Creating Disputes

A dispute can be created for:

* An amount in a customer's account.
* The entire or partial amount of an invoice.
* A specific event in an invoice.
* An unbilled usage event.

After a dispute is created, it is resolved by a user with the appropriate level of security.

To create disputes:

. Retrieve the customer using the _Global Search_ bar.
. Select the customer from the _Customer Results_.
. Click the _View_ icon.
. Choose the associated contact.
. Click the *Validate Contact* button.
. Validate the contact details using _Finger ID Validation_ or _Security Questions_. 

    NOTE: The user can skip the validation by clicking the Skip Validation link.

. On the _Case Panel_, click _Dispute_ issue type.
. Enter the dispute amount.
. Select the _Reason for Dispute_ from the dropdown.
. Add additional documents if necessary or select _Proceed without Additional Documents_ checkbox.
. Add a note.
. Click the *Create Dispute* button.