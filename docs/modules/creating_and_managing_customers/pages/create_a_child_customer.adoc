:imagesdir: ./images

==== Creating a Child Customer

A customer can have multiple child customers. A child customer is created when a parent customer wants to establish a hierarchal structure for reporting or billing (for example, when an organisation wants to report expenses separately for individual regions and departments). Creating child customers enables invoices to be issued individually to each child customer.

Similarly, child customers can be created for residential customers, allowing parents to be invoiced for their children’s service usage.

To create a child customer:

. Retrieve the customer using the _Global Search_ bar.
. Select the customer from the _Customer Results_.
. Click the _View_ icon.
. Choose the associated contact.
. Click the *Validate Contact* button.
. Validate the contact details using _Finger ID Validation_ or _Security Questions_. 

    NOTE: The user can skip the validation by clicking the Skip Validation link.

. On the secondary navigation menu, click _Customer Information_.
. Click the _Customer Hierarchy_ tab.

A new child customer can be created or the existing customers can be added as a child customer.

To add a new child customer:

. On the _Customer Hierarchy_ tab, click the *Add* button.
. Click *Add New Customer* button.
. Choose the customer type.

    NOTE: For individual customer type, select Consumer Customer, and for a corporate customer type, select Corporate Customer.  

. Fill in the _Personal Information_, _Contacts_, and _Billing Address_ details if consumer customer type is selected or select the _Company Type_ and fill in the _Company Details_ if corporate customer type is selected. 
. Click *Continue*. 

To add an existing customers as a child customer:

. On the _Customer Hierarchy_ tab, click the *Add* button.
. Enter the _Customer Name_.
. Click the _Search_ icon.
. Select the customer through radial button.
. Click *Add Associated Customer* button.
