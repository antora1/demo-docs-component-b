:imagesdir: ./images

==== Updating Contact Details

Contact details are updated when the contact informs about a change in address, phone number or email address, etc.

To update the contact details:

. Retrieve the contact using the _Global Search_ bar.
. Click the *View* icon to open the contact information.
. Validate the contact details using _Finger ID Validation_ or _Security Questions_. 

    NOTE: The user can skip the validation by clicking the Skip Validation link.
    
. On the secondary navigation menu, click _Contact Information_
. Click the *Edit* button to edit the contact details.

    NOTE: The contact details in the Overview and Additional Information tabs can be updated.

. Update the contact details.
. Click the *Save* button.