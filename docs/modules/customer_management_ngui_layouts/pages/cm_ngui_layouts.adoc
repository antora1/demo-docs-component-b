:imagesdir: ./images

== Customer Management NGUI Layouts

This section describes layout structures.

image::contact_information.png[Contact Information page]

* Secondary Navigation: TBD
* Tabs: TBD
* Go back: TBD
* Go forward: TBD

image::customer_information.png[Customer Information page]

* Widget: TBD